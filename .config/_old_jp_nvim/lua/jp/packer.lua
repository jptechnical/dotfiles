-- This file can be loaded by calling `lua require('plugins')` from your init.vim
-- you may need to source this file with `:so` in nvim
-- you also may need to clone the repo found in nvim.yml

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'


  use {

	  'nvim-telescope/telescope.nvim', tag = '0.1.0',
	  -- or                            , branch = '0.1.x',
	  requires = { {'nvim-lua/plenary.nvim'} }
  }

--  -- Dracula theme
--  use 'Mofiqul/dracula.nvim'
--
--  -- rose-pine theme
--  use({
--    'rose-pine/neovim',
--    as = 'rose-pine',
--    config = function()
--        vim.cmd('colorscheme rose-pine')
--    end
--})

  use ('nvim-lua/plenary.nvim')
  use ('alpha2phi/lp.nvim')

--  use ('ThePrimeagen/harpoon')
  use ('mbbill/undotree')
  use ('tpope/vim-fugitive')
  use ('tpope/vim-surround')

  use {
	  'VonHeikemen/lsp-zero.nvim',
	  requires = {
		  -- LSP Support
		  {'neovim/nvim-lspconfig'},
		  {'williamboman/mason.nvim'},
		  {'williamboman/mason-lspconfig.nvim'},

		  -- Autocompletion
		  {'hrsh7th/nvim-cmp'},
		  {'hrsh7th/cmp-buffer'},
		  {'hrsh7th/cmp-path'},
		  {'saadparwaiz1/cmp_luasnip'},
		  {'hrsh7th/cmp-nvim-lsp'},
		  {'hrsh7th/cmp-nvim-lua'},

		  -- Snippets
		  {'L3MON4D3/LuaSnip'},
		  -- Snippet Collection (Optional)
		  {'rafamadriz/friendly-snippets'},
	  }
  }

use {
    'glacambre/firenvim',
    run = function() vim.fn['firenvim#install'](0) end 
}

use {
  "folke/which-key.nvim",
  config = function()
    vim.o.timeout = true
    vim.o.timeoutlen = 300
    require("which-key").setup {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
    }
  end
}

use "lukas-reineke/indent-blankline.nvim"

--use "terrortylor/nvim-comment"
--require('nvim_comment').setup()

use "tpope/vim-commentary"

use "nvim-tree/nvim-web-devicons"

use {'akinsho/bufferline.nvim', tag = "v3.*", requires = 'nvim-tree/nvim-web-devicons'}

-- use {
--     'vimwiki/vimwiki',
--     config = function()
--       -- vim.g.vimwiki_list = {
--       --   {
--       --     path = '~/techdocs/',
--       --     syntax = 'markdown',
--       --     ext  = '.md',
--       --   }
--       -- }
--       -- vim.g.vimwiki_ext2syntax = {
--       --   ['.md'] = 'markdown',
--       --   ['.markdown'] = 'markdown',
--       --   ['.mdown'] = 'markdown',
--       -- }
--     end
--   }


  use ('nvim-treesitter/nvim-treesitter', {run = ':TSUpdate'})
  use ('nvim-treesitter/playground')
  use { 'michaelb/sniprun', run = 'sh ./install.sh 1'}



  use({
  "nvim-treesitter/nvim-treesitter-textobjects",
  after = "nvim-treesitter",
  requires = "nvim-treesitter/nvim-treesitter",
  })




  end
  )   
