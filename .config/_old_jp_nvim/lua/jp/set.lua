--vim.opt.guicursor = ""

vim.opt.nu = true
vim.opt.relativenumber = true

vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true

vim.opt.smartindent = true

vim.opt.wrap = true

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.termguicolors = true

vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50

vim.opt.colorcolumn = "80"

vim.g.mapleader = " "

vim.opt.clipboard = "unnamedplus"

vim.opt.cursorline = true

-- vim.g.vimwiki_list = {
--   {
--     path = '~/techdocs/',
--     syntax = 'markdown',
--     ext  = '.md',
--   }
-- }
-- vim.g.vimwiki_ext2syntax = {
--   ['.md'] = 'markdown',
--   ['.markdown'] = 'markdown',
--   ['.mdown'] = 'markdown',
-- }

-- -- Support sniprun in markdown
vim.g.markdown_fenced_languages = { "javascript", "typescript", "bash", "lua", "go", "rust", "c", "cpp" }

interpreter_options = {
    GFM_original = {
        use_on_filetypes = {"markdown", "md", "markdown.pandoc", "rstudio" }
    }
}




-- local lsp_zero = require('lsp-zero')

-- lsp_zero.on_attach(function(client, bufnr)
--   local opts = {buffer = bufnr, remap = false}

--   vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
--   vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, opts)
--   vim.keymap.set("n", "<leader>vws", function() vim.lsp.buf.workspace_symbol() end, opts)
--   vim.keymap.set("n", "<leader>vd", function() vim.diagnostic.open_float() end, opts)
--   vim.keymap.set("n", "[d", function() vim.diagnostic.goto_next() end, opts)
--   vim.keymap.set("n", "]d", function() vim.diagnostic.goto_prev() end, opts)
--   vim.keymap.set("n", "<leader>vca", function() vim.lsp.buf.code_action() end, opts)
--   vim.keymap.set("n", "<leader>vrr", function() vim.lsp.buf.references() end, opts)
--   vim.keymap.set("n", "<leader>vrn", function() vim.lsp.buf.rename() end, opts)
--   vim.keymap.set("i", "<C-h>", function() vim.lsp.buf.signature_help() end, opts)
-- end)

-- require('mason').setup({})
-- require('mason-lspconfig').setup({
--   ensure_installed = {'tsserver', 'rust_analyzer'},
--   handlers = {
--     lsp_zero.default_setup,
--     lua_ls = function()
--       local lua_opts = lsp_zero.nvim_lua_ls()
--       require('lspconfig').lua_ls.setup(lua_opts)
--     end,
--   }
-- })

-- local cmp = require('cmp')
-- local cmp_select = {behavior = cmp.SelectBehavior.Select}

-- cmp.setup({
--   sources = {
--     {name = 'path'},
--     {name = 'nvim_lsp'},
--     {name = 'nvim_lua'},
--   },
--   formatting = lsp_zero.cmp_format(),
--   mapping = cmp.mapping.preset.insert({
--     ['<C-p>'] = cmp.mapping.select_prev_item(cmp_select),
--     ['<C-n>'] = cmp.mapping.select_next_item(cmp_select),
--     ['<C-y>'] = cmp.mapping.confirm({ select = true }),
--     ['<C-Space>'] = cmp.mapping.complete(),
--   }),
-- })
















-- require'sniprun'.setup({
--     interpreter_options = {
--         Generic = {
--             MyPython3Config = {                    -- any key name is ok
--                 supported_filetypes = {"python3"}, -- mandatory
--                 extension = ".py",                 -- recommended, but not mandatory. Sniprun use this to create temporary files

--                 interpreter = "python3",           -- interpreter or compiler (+ options if any)
--                 compiler = "",                     -- one of those MUST be non-empty
--                 },
--             MyMarkdownConfig = {                    -- any key name is ok
--                 supported_filetypes = {"markdown"}, -- mandatory
--                 extension = ".md",                 -- recommended, but not mandatory. Sniprun use this to create temporary files

--                 interpreter = "markdown",           -- interpreter or compiler (+ options if any)
--                 compiler = "",                     -- one of those MUST be non-empty
--                 }
--         }
--     }
-- })
