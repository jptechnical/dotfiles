-- -- Firenvim setup
-- function _G.FirenvimSetup(channel)
--   local channel_info = vim.api.nvim_get_chan_info(channel)
--   if channel_info.client and channel_info.client.name == "Firenvim" then
--     vim.opt.laststatus = 0
--   end
-- end

vim.cmd(
  [[ 
    let g:firenvim_config = { 
      \ 'globalSettings': {
          \ 'alt': 'all',
      \  },
      \ 'localSettings': {
          \ '.*': {
              \ 'cmdline': 'neovim',
              \ 'content': 'text',
              \ 'priority': 0,
              \ 'selector': 'textarea',
              \ 'takeover': 'never',
            \ },
        \ }
    \ }
]]
)
