---@type MappingsTable
local M = {}

M.general = {
  n = {
    [";"] = { ":", "enter command mode", opts = { nowait = true } },
    ["<leader>s"] = { ":SnipRun <CR>", "SnipRun" },
  },
  v = {
    [">"] = { ">gv", "indent"},
  },
}

-- more keybinds!

-- vim.api.nvim_set_keymap(
--   "n",
--   "<leader>s",
--   ":SnipRun<cr>",
--   { noremap = true }
--  )

return M
