#!/usr/bin/bash

# If not running interactively, don't do anything
case $- in
  *i*) ;;
    *) return;;
esac

export TERM=xterm-256color


if [ -f /opt/homebrew/bin/brew ]; then
  eval "$(/opt/homebrew/bin/brew shellenv)"
fi

# BASH_IT stuff
#export BASH_IT="$HOME/.bash_it"
#export BASH_IT_THEME='bobby'
#source $HOME/.bash_it/bash_it.sh

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
# export PS1="\u@\h \[\033[32m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\] $ "
export PS1="\[$(tput setaf 39)\]\u\[$(tput setaf 81)\]@\[$(tput setaf 77)\]\h \[$(tput setaf 226)\]\w \[$(tput sgr0)\]$ "

# # Bash my aws 
# export PATH="$PATH:$HOME/.bash-my-aws/bin"
# if [ -f $HOME/.bash-my-aws/aliases ]; then
#   source ~/.bash-my-aws/aliases
# fi
# complete -C '/usr/local/bin/aws_completer' aws

export PATH="$PATH:${BMA_HOME:-$HOME/.bash-my-aws}/bin"
export BMA_COLUMNISE_ONLY_WHEN_TERMINAL_PRESENT=true
source ${BMA_HOME:-$HOME/.bash-my-aws}/aliases

# For ZSH users, uncomment the following two lines:
# autoload -U +X compinit && compinit
# autoload -U +X bashcompinit && bashcompinit

source ${BMA_HOME:-$HOME/.bash-my-aws}/bash_completion.sh

# AWS Completer
export PATH=/usr/local/bin/aws_completer:$PATH

# Case insensitivity in bash complet
set completion-ignore-case on
set show-all-if-ambiguous on

# Homebrew path for python
export PATH=/usr/local/bin:/usr/local/sbin:${PATH}
export PATH=${PATH}:/Users/=$(whoami)/Library/Python/3.7/bin


# Git autocomplete
source $HOME/.config/bash/git-completion.bash

# Eternal history
export HISTCONTROL=ignoredups:erasedups  # no duplicate entries
export HISTSIZE=1000                   # big big history
export HISTFILESIZE=1000               # big big history
shopt -s histappend                      # append to history, don't overwrite it

# Make Vim default editor
export EDITOR=nvim
export VISUAL=nvim

# Add $MYVIMRC variable
export MYVIMRC=$HOME/.config/nvim/init.vim

# Load aliases
if [ -f $HOME/.config/bash/.aliases ]; then
    source $HOME/.config/bash/.aliases
fi

# Bethel stuff
if [ -f $HOME/git/bethel/dotfiles/.aliases ]; then
    source $HOME/git/bethel/dotfiles/.aliases 
fi

if [ -f $HOME/.profile.d/bethel.sh ];then
    source $HOME/.profile.d/bethel.sh
fi


# Set vim keys
set -o vi

# Setup z which is a quick cd replacement
. $HOME/.config/bash/z.sh

# Bash Insulter
#if [ -f /etc/bash.command-not-found ]; then
#    . /etc/bash.command-not-found
#fi


# Load neovim alias if needed
if [ -f $HOME/nvim.appimage ]; then
    alias nvim="~/nvim.appimage"
fi

# Pyenv
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi

# Suppress MacOS zsh message
export BASH_SILENCE_DEPRECATION_WARNING=1

# Go setup
export GOPATH=/Users/jesse/go
export GOBIN=$GOPATH/bin
PATH=$PATH:$GOPATH:$GOBIN
export GO111MODULE=auto
export PATH

# generate random password
#randompass() {
#LC_CTYPE=C < /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c12;echo;
#}


##function run
#run() {
#    number=$1
#    shift
#    for i in `seq $number`; do
#      $@
#    done
#}

export VAULT_USER="jperry2"

export AWS_VAULT_USER=jperry2

# hide git stash count, I am easily confused
export SCM_GIT_SHOW_STASH_INFO=false

export GPG_TTY=$(tty)
export SOPS_AGE_KEY_FILE=$HOME/.sops/nebula.age
export SOPS_AGE_RECIPIENTS=age104xn5xt46wgg3n27em955qm9wkdwrxafx4crtqgq2fzz6zfn3s7slskqwt

if [ -f "$HOME/.ansible-vault/nebula.vault.key" ]; then
  export ANSIBLE_VAULT_PASSWORD_FILE=~/.ansible-vault/nebula.vault.key
fi

if [ -f "$HOME/.cargo/env" ]; then
  source "$HOME/.cargo/env"
fi
export PATH=$HOME/.local/bin:$PATH

#determines search program for fzf
if type ag &> /dev/null; then
    export FZF_DEFAULT_COMMAND='ag -p ~/.gitignore -g ""'
fi
#refer rg over ag
if type rg &> /dev/null; then
    export FZF_DEFAULT_COMMAND='rg --files --hidden --ignore-file ~/.gitignore'
fi

if [ -f "$HOME/.fzf.bash" ]; then
  source "$HOME/.fzf.bash"
fi


alias assume=". assume"
