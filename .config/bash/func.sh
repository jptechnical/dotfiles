#!/bin/bash

valplus () {
  cat $1 | yq r - -jP > output.json && \
  sfn validate -f output.json --no-processing
}

