#!/usr/bin/bash

# If not running interactively, don't do anything
case $- in
  *i*) ;;
    *) return;;
esac

# Load bash_profile if exists
# disabled so it doesn't go circular from vim
source  ~/.bash_profile
if [ -f "$HOME/.cargo/env" ]; then
  source "$HOME/.cargo/env"
fi

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
eval "$(direnv hook bash)"

export JSII_SILENCE_WARNING_UNTESTED_NODE_VERSION=1
